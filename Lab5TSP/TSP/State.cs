﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TSP
{
    class State
    {
        private double bound;
        private double cost;
        private double[,] matrix;
        private int city;
        private int depth;
        private bool solution;
        private List<int> path; 
        private List<int> children;


        public State()
        {
            children = new List<int>();
            path = new List<int>();
        }

        public State(double bound, double cost, double[,] matrix, int city, int depth, List<int> children, List<int> path)
        {
            this.Bound = bound;
            this.Cost = cost;
            this.Matrix = matrix;
            this.City = city;
            this.Depth = depth;
            this.Path = path;
            this.Children = children;
            if (Children.Count == 0)
            {
                this.Solution = true;
            }
            else
            {
                this.Solution = false;
            }
        }

        public double Bound
        {
            get { return bound; }
            set { bound = value; }
        }

        public double Cost
        {
            get { return cost; }
            set { cost = value; }
        }

        public double[,] Matrix
        {
            get { return matrix; }
            set { matrix = value; }
        }

        public int City
        {
            get { return city; }
            set { city = value; }
        }

        public int Depth
        {
            get { return depth; }
            set { depth = value; }
        }

        public bool Solution 
        {
            get { return solution; }
            set { solution = value; }
        }

        public List<int> Path
        {
            get { return path; }
            set { path = value; }
        }

        public List<int> Children
        {
            get { return children; }
            set { children = value; }
        }

    }
}
