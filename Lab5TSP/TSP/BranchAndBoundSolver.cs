﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TSP
{
    class BranchAndBoundSolver
    {
        PriorityQueue queue;
        double bssfCost;
        double bound;
        double[] minimums;
        List<int> bssfList;
        public ArrayList Route;
        private City[] Cities;
        public TSPSolution bssf;
        int numberOfStates;
        int bssfUpdates;
        int maxStatesStored;
        int prunedStates;

        public BranchAndBoundSolver(TSPSolution bssf, City[] cities)
        {
            bssfUpdates = 0;
            maxStatesStored = 0;
            prunedStates = 0;
            this.Cities = cities;
            Route = new ArrayList();
            queue = new PriorityQueue();
            bssfList = new List<int>();
            bssfList.Add(0);
            
            int index = 0;
            Route.Add(Cities[index]);
            while (Route.Count < Cities.Length)
            {
                int minIndex = 0;
                double minCost = double.PositiveInfinity;
                for (int i = 0; i < Cities.Length; i++)
                {
                    if (i != index)
                    {
                        if (!Route.Contains(Cities[i]))
                        {
                            double cost = Cities[index].costToGetTo(Cities[i]);
                            if (cost < minCost)
                            {
                                minCost = cost;
                                minIndex = i;
                            }
                        }
                    }
                }
                index = minIndex;
                Route.Add(Cities[index]);
                bssfList.Add(index);
            }
            this.bssf = new TSPSolution(Route);
            bssfCost = this.bssf.costOfRoute();

            double[,] initialMatrix = initMatrix();
            minimums = getRowMinimums();

            List<int> childrenStates = new List<int>();
            for (int i = 1; i < Cities.Length; i++)
            {
                childrenStates.Add(i);
            }
            State initialState = new State(0, 0, initialMatrix, 0, 0, childrenStates, new List<int>());
            numberOfStates = 1;
            bound = initialState.Bound;
            queue.Push(initialState, bound);
        }

        public void BranchAndBound()
        {
            DateTime start = DateTime.Now;
            DateTime end = start.AddSeconds(30);

            while (!queue.Empty() && DateTime.Now < end && bssfCost > bound)
            {
                State state = queue.Pop();
                if (state.Bound < bssfCost)
                {
                    List<State> children = getChildren(state);
                    foreach (State child in children)
                    {
                        if (child.Bound < this.bssf.costOfRoute())
                        {
                            if (child.Solution && child.Cost < bssfCost)
                            {
                                bssfUpdates++;
                                bssfCost = child.Cost;
                                bssfList = child.Path;
                            }
                            else
                            {
                                // bound = child.Bound;
                                double childBound = child.Bound;
                                foreach (int childIndex in child.Children)
                                {
                                    childBound += minimums[childIndex];
                                    queue.Push(child, childBound);
                                    if (queue.Count > maxStatesStored)
                                    {
                                        maxStatesStored = queue.Count;
                                    }
                                }
                            }
                        }
                        else
                        {
                            prunedStates++;
                        }
                    }
                }
                else
                {
                    prunedStates++;
                }
            }
            Route.Clear();
            for (int i = 0; i < bssfList.Count; i++)
            {
                Route.Add(Cities[bssfList[i]]);
            }
            this.bssf = new TSPSolution(Route);

            Program.MainForm.tbCostOfTour.Text = " " + bssf.costOfRoute();
            Program.MainForm.tbElapsedTime.Text = " " + (DateTime.Now - start);
            Console.WriteLine("Number of States Created: " + numberOfStates);
            Console.WriteLine("Max Amount of Stored States: " + maxStatesStored);
            Console.WriteLine("Number of States Pruned: " + prunedStates);
            Console.WriteLine("Number of BSSF Updates: " + bssfUpdates);
        }

        private double[,] initMatrix()
        {
            double[,] matrix = new double[Cities.Length, Cities.Length];
            for (int i = 0; i < Cities.Length; i++)
            {
                for (int j = 0; j < Cities.Length; j++)
                {
                    if (i == j)
                    {
                        matrix[i, j] = double.PositiveInfinity;
                    }
                    else
                    {
                        matrix[i, j] = Cities[i].costToGetTo(Cities[j]);
                    }
                }
            }
            return matrix;
        }

        private double[] getRowMinimums()
        {
            double[] minimums = new double[Cities.Length];
            for (int i = 0; i < Cities.Length; i++)
            {
                double minimum = double.PositiveInfinity;
                for (int j = 0; j < Cities.Length; j++)
                {
                    if (i != j)
                    {
                        double cost = Cities[i].costToGetTo(Cities[j]);
                        if (cost < minimum)
                        {
                            minimum = cost;
                        }
                    }
                }
                minimums[i] = minimum;
            }
            return minimums;
        }

        public List<State> getChildren(State state)
        {
            List<State> children = new List<State>();
            foreach (int child in state.Children)
            {
                List<int> childList = new List<int>(state.Children);
                List<int> path = new List<int>(state.Path);
                double cost = Cities[state.City].costToGetTo(Cities[child]);
                double[,] matrix = (double[,])state.Matrix.Clone();

                childList.Remove(child);
                path.Add(state.City);
                

                for (int i = 0; i <= matrix.GetUpperBound(0); i++)
                {
                    matrix[i, state.City] = double.PositiveInfinity;
                }
                State nextState = new State(state.Bound + state.Matrix[state.City, child], state.Cost + cost, matrix, child, state.Depth + 1, childList, path);
                numberOfStates++;
                nextState.Bound += boundingFunction(nextState);

                if (nextState.Solution)
                {
                    nextState.Cost += Cities[nextState.City].costToGetTo(Cities[0]);
                    nextState.Path.Add(nextState.City);
                }
                children.Add(nextState);
            }
            return children;
        }

        private double boundingFunction(State state)
        {
            double bound = 0;
            double rowCount = 0;
            double[,] matrix = state.Matrix;

            List<int> children = new List<int>(state.Children);
            children.Add(state.City);
            

            for (int i = 0; i < children.Count; i++)
            {
                double minCost = double.PositiveInfinity;
                for (int j = 0; j < state.Children.Count; j++)
                {
                    if (matrix[children[i], state.Children[j]] < minCost)
                    {
                        minCost = matrix[children[i], state.Children[j]];
                    }
                }
                if (minCost < double.PositiveInfinity)
                {
                    // Console.WriteLine("Min Cost" + minCost);
                    for (int j = 0; j < state.Children.Count; j++)
                    {
                        // Console.WriteLine("Matrix Cell: " + matrix[children[i], state.Children[j]]);
                        matrix[children[i], state.Children[j]] -= minCost;
                    }
                }
                else
                {
                    rowCount++;
                }
            }
            if (rowCount >= matrix.GetUpperBound(0))
            {
                state.Solution = true;
                state.Cost += Cities[1].costToGetTo(Cities[state.City]);
            }
            if (state.Solution)
            {
                return 0;
            }
            return bound;
        }

    }
}
