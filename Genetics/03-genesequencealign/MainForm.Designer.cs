namespace GeneticsLab
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.dataGridViewResults = new System.Windows.Forms.DataGridView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.processButton = new System.Windows.Forms.ToolStripButton();
            this.SequenceA = new System.Windows.Forms.TextBox();
            this.AlignmentATextBox = new System.Windows.Forms.TextBox();
            this.SequenceB = new System.Windows.Forms.TextBox();
            this.AlignmentBTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResults)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewResults
            // 
            this.dataGridViewResults.AllowUserToAddRows = false;
            this.dataGridViewResults.AllowUserToDeleteRows = false;
            this.dataGridViewResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewResults.BackgroundColor = System.Drawing.SystemColors.Info;
            this.dataGridViewResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewResults.Location = new System.Drawing.Point(12, 28);
            this.dataGridViewResults.Name = "dataGridViewResults";
            this.dataGridViewResults.ReadOnly = true;
            this.dataGridViewResults.Size = new System.Drawing.Size(855, 307);
            this.dataGridViewResults.TabIndex = 0;
            this.dataGridViewResults.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewResults_CellMouseClick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusMessage});
            this.statusStrip1.Location = new System.Drawing.Point(0, 419);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(879, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusMessage
            // 
            this.statusMessage.Name = "statusMessage";
            this.statusMessage.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.processButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(879, 26);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // processButton
            // 
            this.processButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.processButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.processButton.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.processButton.Image = ((System.Drawing.Image)(resources.GetObject("processButton.Image")));
            this.processButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.processButton.Name = "processButton";
            this.processButton.Size = new System.Drawing.Size(59, 23);
            this.processButton.Text = "Process";
            this.processButton.Click += new System.EventHandler(this.processButton_Click);
            // 
            // SequenceA
            // 
            this.SequenceA.Font = new System.Drawing.Font("Lucida Sans Typewriter", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SequenceA.Location = new System.Drawing.Point(12, 341);
            this.SequenceA.Name = "SequenceA";
            this.SequenceA.ReadOnly = true;
            this.SequenceA.Size = new System.Drawing.Size(429, 22);
            this.SequenceA.TabIndex = 3;
            this.SequenceA.Text = "Sequence A";
            // 
            // AlignmentATextBox
            // 
            this.AlignmentATextBox.BackColor = System.Drawing.SystemColors.Control;
            this.AlignmentATextBox.Font = new System.Drawing.Font("Lucida Sans Typewriter", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AlignmentATextBox.Location = new System.Drawing.Point(12, 367);
            this.AlignmentATextBox.Name = "AlignmentATextBox";
            this.AlignmentATextBox.Size = new System.Drawing.Size(855, 22);
            this.AlignmentATextBox.TabIndex = 4;
            this.AlignmentATextBox.Text = "Sequence A Alignment";
            // 
            // SequenceB
            // 
            this.SequenceB.Font = new System.Drawing.Font("Lucida Sans Typewriter", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SequenceB.Location = new System.Drawing.Point(447, 341);
            this.SequenceB.Name = "SequenceB";
            this.SequenceB.ReadOnly = true;
            this.SequenceB.Size = new System.Drawing.Size(420, 22);
            this.SequenceB.TabIndex = 5;
            this.SequenceB.Text = "Sequence B";
            // 
            // AlignmentBTextBox
            // 
            this.AlignmentBTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.AlignmentBTextBox.Font = new System.Drawing.Font("Lucida Sans Typewriter", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AlignmentBTextBox.Location = new System.Drawing.Point(12, 393);
            this.AlignmentBTextBox.Name = "AlignmentBTextBox";
            this.AlignmentBTextBox.Size = new System.Drawing.Size(855, 22);
            this.AlignmentBTextBox.TabIndex = 6;
            this.AlignmentBTextBox.Text = "Sequence B Alignment";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 441);
            this.Controls.Add(this.AlignmentBTextBox);
            this.Controls.Add(this.SequenceB);
            this.Controls.Add(this.AlignmentATextBox);
            this.Controls.Add(this.SequenceA);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.dataGridViewResults);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "CS 312 - Project #3 - Gene Sequence Alignment";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResults)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewResults;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusMessage;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton processButton;
        private System.Windows.Forms.TextBox SequenceA;
        private System.Windows.Forms.TextBox AlignmentATextBox;
        private System.Windows.Forms.TextBox SequenceB;
        private System.Windows.Forms.TextBox AlignmentBTextBox;
    }
}

