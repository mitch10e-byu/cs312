﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneticsLab
{
    class Node
    {
        private Node Previous;
        private String A;
        private String B;
        private int Cost;

        public Node()
        {
            this.Previous = null;
            this.A = "";
            this.B = "";
            this.Cost = -1;
        }

        public Node(Node p, String a, String b, int cost)
        {
            this.Previous = p;
            this.A = a;
            this.B = b;
            this.Cost = cost;
        }

        public Node previous
        {
            get { return Previous; }
            set { this.Previous = previous; }
        }

        public String a
        {
            get { return A; }
            set { this.A = a; }
        }

        public String b
        {
            get { return B; }
            set { this.B = b; }
        }

        public int cost
        {
            get { return Cost; }
            set { this.Cost = cost; }
        }

    }
}
