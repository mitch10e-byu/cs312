using System;
using System.Collections.Generic;
using System.Text;

namespace GeneticsLab
{
    class PairWiseAlign
    {

        /// <summary>
        /// Align only 5000 characters in each sequence.
        /// </summary>
        /// Changed MaxCharactersToAlign from 5000 to use the offset with the algorithm.
        private int MaxCharactersToAlign = 5001;
        private int match = -3;
        private int sub = 1;
        private int indel = 5;

        /// <summary>
        /// this is the function you implement.
        /// </summary>
        /// <param name="sequenceA">the first sequence</param>
        /// <param name="sequenceB">the second sequence, may have length not equal to the length of the first seq.</param>
        /// <param name="resultTableSoFar">the table of alignment results that has been generated so far using pair-wise alignment</param>
        /// <param name="rowInTable">this particular alignment problem will occupy a cell in this row the result table.</param>
        /// <param name="columnInTable">this particular alignment will occupy a cell in this column of the result table.</param>
        /// <returns>the alignment score for sequenceA and sequenceB.  The calling function places the result in entry rowInTable,columnInTable
        /// of the ResultTable</returns>
        public int Align(GeneSequence sequenceA, GeneSequence sequenceB, ResultTable resultTableSoFar, int rowInTable, int columnInTable)
        {
            if (rowInTable >= columnInTable)
            {
                return (int)resultTableSoFar.GetCell(columnInTable, rowInTable);
            }
            List<int> previousRow = new List<int>();
            List<int> currentRow = new List<int>();

            string aSeqOffset = "-" + sequenceA.Sequence;
            string bSeqOffset = "-" + sequenceB.Sequence;

            int aLength = Math.Min(aSeqOffset.Length, MaxCharactersToAlign);
            int bLength = Math.Min(bSeqOffset.Length, MaxCharactersToAlign);

            for (int row = 0; row < aLength; row++)
            {
                for (int column = 0; column < bLength; column++)
                {
                    // Default Top Left Corner = 0
                    int cost = 0;
                    if (row == 0 && column == 0)
                    {
                        cost = 0;
                    }
                    // Init Top Row
                    else if (row == 0 && column > 0)
                    {
                        cost = currentRow[column - 1] + indel;
                    }
                    // Init Left Column
                    else if (row > 0 && column == 0)
                    {
                        cost = previousRow[column] + indel;
                    }
                    else
                    {
                        int up = previousRow[column] + indel;
                        int left = currentRow[column - 1] + indel;
                        int diagonal;
                        if (aSeqOffset[row] == bSeqOffset[column])
                        {
                            diagonal = previousRow[column - 1] + match;
                        }
                        else
                        {
                            diagonal = previousRow[column - 1] + sub;
                        }
                        cost = Math.Min(Math.Min(up, left), diagonal);
                    }
                    currentRow.Add(cost);
                }
                // O(n) space by only keeping one row at a time of n length.
                previousRow = currentRow;
                currentRow = new List<int>();
            }

            resultTableSoFar.SetCell(columnInTable, rowInTable, (previousRow[previousRow.Count - 1]));

            return previousRow[previousRow.Count - 1];

            // a place holder computation.  You'll want to implement your code here. 
            //return (Math.Abs(sequenceA.Sequence.Length - sequenceB.Sequence.Length));             
        }


        public void Extract(GeneSequence sequenceA, GeneSequence sequenceB, System.Windows.Forms.TextBox boxA, System.Windows.Forms.TextBox boxB, int rowInTable, int columnInTable, ResultTable resultTableSoFar)
        {
            if (rowInTable == columnInTable)
            {
                boxA.Text = "Same Gene Sequence";
                boxB.Text = "Same Gene Sequence";
                return;
            }

            List<Node> previousRow = new List<Node>();
            List<Node> currentRow = new List<Node>();

            string aSeqOffset = "-" + sequenceA.Sequence;
            string bSeqOffset = "-" + sequenceB.Sequence;

            int aLength = Math.Min(aSeqOffset.Length, MaxCharactersToAlign);
            int bLength = Math.Min(bSeqOffset.Length, MaxCharactersToAlign);

            for (int row = 0; row < aLength; row++)
            {
                for (int column = 0; column < bLength; column++)
                {
                    // Default Top Left Corner = 0
                    Node node = null;
                    if (row == 0 && column == 0)
                    {
                        node = new Node(null, "-", "-", 0);
                    }
                    // Init Top Row
                    else if (row == 0 && column > 0)
                    {
                        node = new Node(currentRow[column - 1], "-", bSeqOffset[column].ToString(), (currentRow[column - 1].cost + indel));
                    }
                    // Init Left Column
                    else if (row > 0 && column == 0)
                    {
                        node = new Node(previousRow[column], aSeqOffset[row].ToString(), "-", (previousRow[column].cost + indel));
                    }
                    else
                    {
                        Node up = new Node(previousRow[column], aSeqOffset[row].ToString(), "-", (previousRow[column].cost + indel));
                        Node left = new Node(currentRow[column - 1], "-", bSeqOffset[column].ToString(), (currentRow[column - 1].cost + indel));
                        Node diagonal = null;
                        if (aSeqOffset[row] == bSeqOffset[column])
                        {
                            diagonal = new Node(previousRow[column - 1], aSeqOffset[row].ToString(), bSeqOffset[column].ToString(), (previousRow[column - 1].cost + match));
                        }
                        else
                        {
                            diagonal = new Node(previousRow[column - 1], aSeqOffset[row].ToString(), bSeqOffset[column].ToString(), (previousRow[column - 1].cost + sub));
                        }
                        node = smallestCost(up, left, diagonal);
                    }
                    currentRow.Add(node);
                }
                previousRow = currentRow;
                currentRow = new List<Node>();
            }

            applyExtraction(previousRow[previousRow.Count - 1], boxA, boxB);

        }

        private Node smallestCost(Node up, Node left, Node diagonal)
        {
            Node smallest = up;
            if (left.cost < smallest.cost)
            {
                smallest = left;
            }
            if (diagonal.cost < smallest.cost)
            {
                smallest = diagonal;
            }
            return smallest;
        }

        private void applyExtraction(Node smallest, System.Windows.Forms.TextBox boxA, System.Windows.Forms.TextBox boxB)
        {
            // boxA.Text = "Apply Extraction";
            // boxB.Text = "Apply Extraction";
            StringBuilder builderA = new StringBuilder();
            StringBuilder builderB = new StringBuilder();

            while (smallest.previous != null)
            {
                builderA.Append(smallest.a);
                builderB.Append(smallest.b);
                smallest = smallest.previous;
            }

            char[] sequenceA = builderA.ToString().ToCharArray();
            char[] sequenceB = builderB.ToString().ToCharArray();
            Array.Reverse(sequenceA);
            Array.Reverse(sequenceB);
            String seqA = new String(sequenceA);
            String seqB = new String(sequenceB);

            int maxLength = 100;
            if(seqA.Length <= maxLength) {
                boxA.Text = seqA;
            }
            else
            {
                boxA.Text = seqA.Substring(0, maxLength);
            }

            if (seqB.Length <= maxLength)
            {
                boxB.Text = seqB;
            }
            else
            {
                boxB.Text = seqB.Substring(0, maxLength);
            }
           
            

        }
    }
}