﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace FermatPrimalityTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void solve_click(object sender, EventArgs e)
        {
            // Grab the Input values from the form
            int inputField = Convert.ToInt32(input.Text);
            Int32 kVal = Convert.ToInt32(kValue.Text);

            //create a variable which will be the overall result of the fermat primality test
            bool isPrime = fermatTest(inputField, kVal);

            if (isPrime)
            {
                output.Text = "yes with probability " + (1-(1/(Math.Pow(2, kVal))));
            }
            else
            {
                output.Text = "no";
            }
            
        }

        private bool fermatTest(int input, Int32 k)
        {
            //create random number generator
            Random random = new Random();
            
            //start off assuming that the input value is prime for looping simplicity
            bool isPrime = true;
            for (int i = 0; i < k; i++)
            {
                //  Fermat Theorem 
                //  a ^ (p - 1) = 1 ( mod p )
                // Constraint: 1 <= a < input
                // a = 1 does not work for this algorithm. 
                // randomly assign a to a number between 2 & input - 1
                int a = random.Next(2, (input - 1));
                if (modularExponentiation(a, input - 1, input) != 1)
                {
                    // Found an instance where input is composite. Set isPrime to false.
                    isPrime = false;

                    //For faster algorithm, return here could be placed.
                } 

            }
                return isPrime;
        }

        private BigInteger modularExponentiation(BigInteger x, double y, BigInteger n)
        {
            // Modular Exponentiation implementation from psuedocode
            if (y == 0)
            {
                return 1;
            }

            // Recursive version of modular exponentiation
            // to iterate is human, to recurse, divine.
            BigInteger z = modularExponentiation(x, Math.Floor(y / 2), n);
            if (y % 2 == 0)
            {
                //ifValue is used for debugging to see what is happening
                BigInteger ifValue = (z*z) % n;
                return ifValue;
            }
            //elseValue is used for debugging to see what is happening
            BigInteger elseValue = x * (z*z) % n;
            return elseValue;
        }
    }
}
