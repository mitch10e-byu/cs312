﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace NetworkRouting
{
    class PriorityQueue
    {
        public int[] pointers;
        public Node[] heap;
        public int counter;
        public int totalSize;
        public List<Node> nodes;

        public PriorityQueue(int size)
        {
            this.totalSize = size;
            this.pointers = new int[size + 1];
            this.heap = new Node[size + 1];
            counter = 0;
            for (int i = 0; i < size + 1; i++)
            {
                pointers[i] = -1;
                heap[i] = null;
            }
        }

        public PriorityQueue(List<Node> nodes)
        {
            int size = nodes.Count;
            this.nodes = nodes;
            this.totalSize = size;
            this.pointers = new int[size + 1];
            this.heap = new Node[size + 1];
            counter = 0;
            for (int i = 0; i < size + 1; i++)
            {
                pointers[i] = -1;
                heap[i] = null;
            }
        }

        public void make(List<PointF> points)
        {
            foreach (PointF point in points)
            {
                counter++;
                Node n = new Node(point, counter);
                heap[counter] = n;
                pointers[counter] = counter;
                
            }
        }

        public Boolean isEmpty()
        {
            return (counter == 0);
        }

        public void insert(int index, Node node, double distance, Node prev)
        {
            index = index + 1;
            counter++;
            pointers[index] = counter;
            node.distance = distance;
            node.prev = prev;
            heap[counter] = node;
            bubbleUp(counter);
        }

        public Node deleteMin()
        {
            Node minNode = heap[1];
            pointers[minNode.pointerIndex] = -1;
            if (counter != 1)
            {
                heap[1] = heap[counter];
                pointers[heap[1].pointerIndex] = 1;
            }
            heap[counter] = null;
            counter--;
            bubbleDown();
            return minNode;
        }

        private void bubbleDown()
        {
            int index = 1;
            while (hasLeftChild(index))
            {
                int smallerChild = index * 2;
                if (hasRightChild(index))
                {
                    if (heap[index * 2].distance > heap[(index * 2) + 1].distance)
                    {
                        smallerChild = (index * 2) + 1;
                    }
                }
                if (heap[index].distance > heap[smallerChild].distance)
                {
                    swap(smallerChild, index);
                }
                else
                {
                    break;
                }
                index = smallerChild;
            }
        }

        private bool hasLeftChild(int index)
        {
            if (index * 2 > totalSize)
            {
                return false;
            }
            return heap[index * 2] != null;
        }

        private bool hasRightChild(int index)
        {
            if (index * 2 > totalSize)
            {
                return false;
            }
            return heap[(index * 2) + 1] != null;
        }

        public void decreaseKey(int index, double distance)
        {
            index = index + 1;
            int heapIndex = pointers[index];
            if (distance < heap[heapIndex].distance || heap[heapIndex].distance == Double.PositiveInfinity)
            {
                heap[heapIndex].distance = distance;
            }
            bubbleUp(index);
        }

        private void bubbleUp(int index)
        {
            if (index == 1)
            {
                return;
            }
            bool bubbled = true;
            while (bubbled)
            {
                bubbled = false;
                double heapIndex = pointers[index];
                if (heapIndex <= 1)
                {
                    break;
                }
                double parent = Math.Floor(heapIndex / 2);
                if (heap[(int)heapIndex].distance < heap[(int)parent].distance)
                {
                    swap((int)heapIndex, (int)parent);
                    bubbled = true;
                }
            }
        }

        private void swap(int heapIndex, int parent)
        {
            Node tempNode = heap[heapIndex];
            heap[heapIndex] = heap[parent];
            heap[parent] = tempNode;
            pointers[heap[heapIndex].pointerIndex] = heapIndex;
            pointers[heap[parent].pointerIndex] = parent;
        }
    }

    public class Node
    {
        public PointF point;
        public int pointerIndex;
        public Double distance;
        public Node prev;
        public Boolean visited;

        public Node(PointF point, int index)
        {
            this.point = point;
            this.pointerIndex = index;
            this.prev = null;
            this.distance = Double.PositiveInfinity;
            this.visited = false;
        }

    }

}
