﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace NetworkRouting
{
    class NetworkSolver
    {

        public int startNodeIndex = -1;
        public int stopNodeIndex = -1;
        public List<PointF> points;
        public Graphics graphics;
        public List<HashSet<int>> adjacencyList;
        public double distanceTraveled;
        public Pen pen;

        public NetworkSolver(List<PointF> points, Graphics graphics, List<HashSet<int>> adjacencyList, int startNodeIndex, int stopNodeIndex)
        {
            this.pen = new Pen(Color.LimeGreen, 4);
            this.startNodeIndex = startNodeIndex;
            this.stopNodeIndex = stopNodeIndex;
            this.points = points;
            this.graphics = graphics;
            this.adjacencyList = adjacencyList;

            if (points == null || graphics == null || adjacencyList == null)
            {
                throw new ArgumentNullException("Click Generate Before Solve");
            }

            if (startNodeIndex == -1 || stopNodeIndex == -1)
            {
                throw new IndexOutOfRangeException("Select 2 Points Before Solve");
            }
        }

        public TimeSpan solveAllPaths()
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            PriorityQueue queue = new PriorityQueue(points.Count);
            queue.make(points);
            queue.decreaseKey(startNodeIndex, 0);
            Node minNode = null;
            List<Node> nodes = new List<Node>();
            while (!queue.isEmpty())
            {
                minNode = queue.deleteMin();
                HashSet<int> adjacencies = adjacencyList[minNode.pointerIndex - 1];
                foreach (int edge in adjacencies)
                {
                    if (queue.pointers[edge + 1] != -1) { 
                        Node endpoint = queue.heap[queue.pointers[edge + 1]];
                        if (endpoint.distance > (minNode.distance + distance(minNode.point, endpoint.point)))
                        {
                            endpoint.distance = (minNode.distance + distance(minNode.point, endpoint.point));
                            endpoint.prev = minNode;
                            queue.decreaseKey(edge, endpoint.distance);
                        }
                    }
                }
                nodes.Add(minNode);
            }
            Node endNode = null;
            foreach (Node n in nodes)
            {
                if (n.pointerIndex - 1 == this.stopNodeIndex)
                {
                    endNode = n;
                }
            }
            drawPath(endNode);
            distanceTraveled = endNode.distance;


            timer.Stop();
            return timer.Elapsed;
        }

       
        public TimeSpan solveOnePath()
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            List<Node> nodes = new List<Node>();
            int counter = 1;
            foreach (PointF point in points)
            {
                Node n = new Node(point, counter);
                counter++;
                nodes.Add(n);
            }
           
            Node destination = nodes[stopNodeIndex];
            PriorityQueue queue = new PriorityQueue(nodes);
            queue.insert(startNodeIndex, queue.nodes[startNodeIndex], 0, null);

            bool foundPath = false;

            Node minNode = null;
            while (!queue.isEmpty())
            {
                minNode = queue.deleteMin();
                minNode.visited = true;
                if (minNode == destination)
                {
                    foundPath = true;
                    break;
                }
                HashSet<int> adjacencies = adjacencyList[minNode.pointerIndex - 1];
                foreach (int edge in adjacencies)
                {
                    Node nextNode = queue.nodes[edge];
                    if (!nextNode.visited) {
                        if (nextNode.distance == Double.PositiveInfinity)
                        {
                            queue.insert(edge, nextNode, (minNode.distance + distance(minNode.point, nextNode.point)), minNode);
                        }
                        else
                        {

                            if (nextNode.distance > minNode.distance + distance(minNode.point, nextNode.point))
                            {
                                nextNode.distance = minNode.distance + distance(minNode.point, nextNode.point);
                                nextNode.prev = minNode;
                                queue.decreaseKey(edge, nextNode.distance);
                            }
                        }
                    }
                }
            }
            if (foundPath)
            {
                pen.Color = Color.Red;
                pen.Width = 2;
                drawPath(minNode);
                distanceTraveled = minNode.distance;
            }
            else
            {
                distanceTraveled = Double.PositiveInfinity;
            }
            timer.Stop();
            return timer.Elapsed;
        }

        private void drawPath(Node n)
        {
            while (n.prev != null)
            {
                graphics.DrawLine(pen, n.point, n.prev.point);
                n = n.prev;
            }
            
        }


        public double distance(PointF source, PointF destination)
        {
            return Math.Sqrt(Math.Pow((source.X - destination.X), 2) + Math.Pow((source.Y - destination.Y), 2));
        }
    }
}
