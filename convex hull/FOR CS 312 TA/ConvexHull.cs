﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace convexHull
{
    class ConvexHull
    {

        public List<HullNode> nodes;
        public HullNode leftNode;
        public HullNode rightNode;
        public Graphics g;
        public Pen pen;

        public ConvexHull(Graphics g, Pen pen)
        {
            this.g = g;
            this.pen = pen;
            nodes = new List<HullNode>();
        }

        public void makeHull(ref List<PointF> points)
        {
            if (points.Count == 2)
            {
                leftNode = new HullNode();
                leftNode.point = points[0];
                rightNode = new HullNode();
                rightNode.point = points[1];
                leftNode.next = rightNode;
                leftNode.prev = rightNode;
                rightNode.next = leftNode;
                rightNode.prev = leftNode;

                nodes.Add(leftNode);
                nodes.Add(rightNode);

            }
            else if (points.Count == 3)
            {
                leftNode = new HullNode();
                leftNode.point = points[0];
                HullNode middle = new HullNode();
                middle.point = points[1];
                rightNode = new HullNode();
                rightNode.point = points[2];
                double slope1 = calculateSlope(leftNode.point, middle.point);
                double slope2 = calculateSlope(leftNode.point, rightNode.point);
                if (slope1 > slope2)
                {
                    leftNode.next = middle;
                    middle.next = rightNode;
                    rightNode.next = leftNode;

                    leftNode.prev = rightNode;
                    rightNode.prev = middle;
                    middle.prev = leftNode;

                    nodes.Add(leftNode);
                    nodes.Add(middle);
                    nodes.Add(rightNode);
                }
                else if (slope1 < slope2)
                {
                    leftNode.next = rightNode;
                    rightNode.next = middle;
                    middle.next = leftNode;

                    leftNode.prev = middle;
                    middle.prev = rightNode;
                    rightNode.prev = leftNode;

                    nodes.Add(leftNode);
                    nodes.Add(rightNode);
                    nodes.Add(middle);
                }
                else
                {
                    leftNode = new HullNode();
                    leftNode.point = points[0];
                    rightNode = new HullNode();
                    rightNode.point = points[1];
                    leftNode.next = rightNode;
                    leftNode.prev = rightNode;
                    rightNode.next = leftNode;
                    rightNode.prev = leftNode;

                    nodes.Add(leftNode);
                    nodes.Add(rightNode);
                }

            }
        }

        public double calculateSlope(PointF a, PointF b)
        {
            double rise = (a.Y - b.Y);
            double run = (a.X - b.X);
            return -rise / run;
        }

        public String print()
        {
            String output = "Count: " + nodes.Count;
            for (int i = 0; i < nodes.Count; i++)
            {
                output += "\nNode[" + i + "]\t" + nodes[i].point.X;
            }
            return output;
        }

        public void drawHull()
        {
            if (nodes.Count < 2)
            {
                return;
            }
            for (int i = 0; i < nodes.Count - 1; i++)
            {
                g.DrawLine(pen, nodes[i].point, nodes[i + 1].point);
            }
            g.DrawLine(pen, nodes[(nodes.Count - 1)].point, nodes[0].point);

        }

        public void drawHullFromLinks()
        {
            HullNode currentNode = this.leftNode;
            while (currentNode.point.X != this.rightNode.point.X)
            {
                g.DrawLine(pen, currentNode.point, currentNode.next.point);
                currentNode = currentNode.next;

            }
            while (currentNode.point.X != this.leftNode.point.X)
            {
                g.DrawLine(pen, currentNode.point, currentNode.next.point);
                currentNode = currentNode.next;
            }
        }

        public void createMergedNodeList(ref ConvexHull hull)
        {
            HullNode currentNode = hull.leftNode;
            hull.nodes.Add(currentNode);
            currentNode = currentNode.next;
            int count = 0;
            while (currentNode.point.X != hull.rightNode.point.X)
            {
                count++;
                if (count > 100)
                {
                    //break;
                }
                hull.nodes.Add(currentNode);
                currentNode = currentNode.next;
            }

        }

    }
    class HullNode
    {
        public PointF point;
        public HullNode next;
        public HullNode prev;
    }


    
}
