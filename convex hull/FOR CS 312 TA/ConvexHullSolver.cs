using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using convexHull;

namespace _2_convex_hull
{
    class ConvexHullSolver
    {
        System.Drawing.Graphics g;
        System.Windows.Forms.PictureBox pictureBoxView;
        Pen pen;
        HullNode topLeftTangent;
        HullNode topRightTangent;
        HullNode bottomLeftTangent;
        HullNode bottomRightTangent;

        public ConvexHullSolver(System.Drawing.Graphics g, System.Windows.Forms.PictureBox pictureBoxView)
        {
            this.pen = new Pen(Color.Maroon, 1F);
            this.g = g;
            this.pictureBoxView = pictureBoxView;
        }
        public void Refresh()
        {
            // Use this especially for debugging and whenever you want to see what you have drawn so far
            pictureBoxView.Refresh();
        }
        public void Pause(int milliseconds)
        {
            // Use this especially for debugging and to animate your algorithm slowly
            pictureBoxView.Refresh();
            System.Threading.Thread.Sleep(milliseconds);
        }

        private void BruteForceSolve(List<PointF> points)
        {

            if (points.Count == 3)
            {
                g.DrawLine(pen, points[0], points[1]);
                g.DrawLine(pen, points[1], points[2]);
                g.DrawLine(pen, points[2], points[0]);
            }
            else if (points.Count == 2)
            {
                g.DrawLine(pen, points[0], points[1]);
            }
        }

        public void Solve(List<System.Drawing.PointF> points)
        {
            points.Sort((p1, p2) => (p1.X.CompareTo(p2.X)));
            if (points.Count <= 3)
            {
                BruteForceSolve(points);
            }
            else
            {
                ConvexHull convexHull = makeHull(points);
                drawHullFromLinks(convexHull);
            }

        }

        public void drawHullFromLinks(ConvexHull hull)
        {
            pen.Color = Color.RoyalBlue;
            HullNode currentNode = hull.leftNode;
            while (currentNode.point.X != hull.rightNode.point.X)
            {
                g.DrawLine(pen, currentNode.point, currentNode.next.point);
                currentNode = currentNode.next;
            }
            while (currentNode.point.X != hull.leftNode.point.X)
            {
                 g.DrawLine(pen, currentNode.point, currentNode.next.point);
                 currentNode = currentNode.next;
            }
        }

        private ConvexHull makeHull(List<PointF> points)
        {
            ConvexHull hull = null;
            if (points.Count <= 3)
            {
                hull = new ConvexHull(g, pen);
                hull.makeHull(ref points);
                //pen.Color = Color.DarkSalmon;
                //hull.drawHull();
                //Pause(1);
                return hull;
            }



            List<PointF> leftPoints;
            List<PointF> rightPoints;
            if (points.Count % 2 == 0)
            {
                leftPoints  = points.GetRange(0, (points.Count / 2));
                rightPoints = points.GetRange((points.Count / 2), (points.Count / 2));
            }
            else
            {
                leftPoints  = points.GetRange(0, (points.Count / 2) + 1);
                rightPoints = points.GetRange((points.Count / 2) + 1, (points.Count / 2));
            }
            if (points.Count % 2 == 0)
            {
                
            }
            else
            {
                
            }
            ConvexHull leftHull = makeHull(leftPoints);
            ConvexHull rightHull = makeHull(rightPoints);

            return mergeHulls(leftHull, rightHull);
        }

        public ConvexHull mergeHulls(ConvexHull left, ConvexHull right)
        {
            ConvexHull hull = new ConvexHull(this.g, this.pen);
            hull.leftNode = left.leftNode;
            hull.rightNode = right.rightNode;
            findUpperTangent(left.rightNode, right.leftNode, hull);
            findLowerTangent(left.rightNode, right.leftNode, hull);
            combineTangents();
            return hull;
        }

        public void combineTangents()
        {
            topLeftTangent.next = topRightTangent;
            topRightTangent.prev = topLeftTangent;
            bottomRightTangent.next = bottomLeftTangent;
            bottomLeftTangent.prev = bottomRightTangent;
        }

        public void findUpperTangent(HullNode a, HullNode b, ConvexHull hull)
        {
            
            bool runAgain = true;
            while (runAgain)
            {
                runAgain = false;
                bool aMoved = true;
                bool bMoved = true;
                while (aMoved)
                {
                    double slope1 = slope(a, b);
                    double slope2 = slope(a.prev, b);
                    if (slope1 > slope2)
                    {
                        if (a.point.X == hull.leftNode.point.X)
                        {
                            aMoved = false;
                        }
                        else
                        {
                            a = a.prev;
                        }
                    }
                    else
                    {
                        aMoved = false;
                    }
                } 
                while (bMoved)
                {
                    double slope1 = slope(a, b);
                    double slope2 = slope(a, b.next);
                    if (slope1 < slope2)
                    {
                        if (b.point.X == hull.rightNode.point.X)
                        {
                            bMoved = false;
                        }
                        else
                        {
                            b = b.next;
                            runAgain = true;
                        }
                    }
                    else
                    {
                        bMoved = false;
                    }
                }
            }
            topLeftTangent = a;
            topRightTangent = b;            
        }

        public void findLowerTangent(HullNode a, HullNode b, ConvexHull hull)
        {
            bool runAgain = true;
            while (runAgain)
            {
                runAgain = false;
                bool aMoved = true;
                bool bMoved = true;
                
                while (aMoved)
                {
                    double slope1 = slope(a, b);
                    double slope2 = slope(a.next, b);
                    if (slope1 < slope2)
                    {
                        if (a.point.X == hull.leftNode.point.X)
                        {
                            aMoved = false;
                        }
                        else
                        {
                            a = a.next;
                        }
                    }
                    else
                    {
                        aMoved = false;
                    }
                }
                while (bMoved)
                {
                    double slope1 = slope(a, b);
                    double slope2 = slope(a, b.prev);
                    if (slope1 > slope2)
                    {
                        if (b.point.X == hull.rightNode.point.X)
                        {
                            bMoved = false;
                        }
                        else
                        {
                            b = b.prev;
                            runAgain = true;
                        }
                    }
                    else
                    {
                        bMoved = false;
                    }
                }
            }
            bottomLeftTangent = a;
            bottomRightTangent = b;
        }

        public double slope(PointF a, PointF b)
        {
            double rise = (a.Y - b.Y);
            double run = (a.X - b.X);
            return -rise / run;
        }

        public double slope(HullNode a, HullNode b)
        {
            double rise = (a.point.Y - b.point.Y);
            double run = (a.point.X - b.point.X);
            return -rise / run;
        }
    }
}
